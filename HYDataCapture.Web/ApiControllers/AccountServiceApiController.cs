﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HYDataCapture.Web.ApiControllers
{
    [RoutePrefix("api/AccountServiceApi")]
    public class AccountServiceApiController : ApiController
    {
        [Route("Login"), HttpPost]
        public Domain.Dtos.LoginOutputDto Login(Domain.Dtos.LoginInputDto input)
        {
            var output = new Domain.Dtos.LoginOutputDto() { Message = "帐号或密码错误" };
            if (input.UserName == "admin" && input.Password == "admin")
            {
                output.Succeed = true;
                output.Message = "";
            }
            return output;
        }
    }
}
