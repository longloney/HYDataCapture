﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HYDataCapture.Utility.DB;

namespace HYDataCapture.Utility.Helpers
{
    public static  class SqliteRepositoryHelper
    {
        public static void Execute(Action<SqliteRepository> func)
        {
            SqliteRepository rep = null;
            try
            {
                rep = new SqliteRepository();
                func?.Invoke(rep);
            }
            finally
            {
                if (rep != null)
                {
                    rep.Dispose();
                    rep = null;
                }
            }
        }
    }
}
