﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace HY.Utility.Net
{
    public class HttpHead
    {
        public string Url { get; set; }
        public string Method { get; set; }
        public string Cookie { get; set; }
        public string Data { get; set; }

        public string ContentType { get; set; }
        public string Referer { get; set; }
        public bool KeepAlive { get; set; }
        public bool Ajax { get; set; }
        public Dictionary<string, string> ExtendHead { get; set; }
        public bool AllowAutoRedirect { get; set; }

        public string Result_Exception { get; set; }
        public HttpStatusCode Result_StatusCode { get; set; }
        public string Result_RedirectUrl { get; set; }
        public string Result_SetCookie { get; set; }

        public bool Result_Success { get; set; }
    }
}
