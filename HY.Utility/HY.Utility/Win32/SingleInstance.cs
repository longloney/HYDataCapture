﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace HY.Utility.Win32
{
    public static class SingleInstance
    {
        public static EventWaitHandle ProgramStarted { get; set; }

        /// <summary>
        /// 是否在创建新实例
        /// </summary>
        /// <returns></returns>
        public static bool IsCreateNew()
        {
            bool createNew;
            ProgramStarted = new EventWaitHandle(false, EventResetMode.AutoReset, System.Diagnostics.Process.GetCurrentProcess().ProcessName, out createNew);
            if (!createNew)
            {
                ProgramStarted.Set();
            }
            return createNew;
        }

        public static void SetCallback(WaitOrTimerCallback callback, object state)
        {
            ThreadPool.RegisterWaitForSingleObject(ProgramStarted, callback, state, -1, false);
        }
    }
}
