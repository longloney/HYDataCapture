﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StackExchange.Redis;

namespace HY.Utility.Cache
{
    public static class RedisHelper
    {
        private static  ConnectionMultiplexer _Redis;
        private readonly static IDatabase _Database;
        static RedisHelper()
        {
            _Redis = ConnectionMultiplexer.Connect("localhost:6379");
            _Database = _Redis.GetDatabase();
        }

        public static void Set(string key,string value)
        {
            _Database.StringSet(key, value);
        }

        public static string Get(string key)
        {
            return _Database.StringGet(key);
        }

        public static void Disponse()
        {
            if (_Redis != null)
            {
                _Redis.Dispose();
                _Redis = null;
            }
        }
    }
}
