﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HY.Utility.Match
{
    public class AttributeValue
    {
        public string Attribute { get; set; }
        public string Value { get; set; }
    }
}
