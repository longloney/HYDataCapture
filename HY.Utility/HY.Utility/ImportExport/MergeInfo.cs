﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace HY.Utility.ImportExport
{
    public class MergeInfo
    {
        public Point StartCell { get; set; }
        public Point EndCell { get; set; }
        public string Text { get; set; }
        public int SheetIndex { get; set; }
    }
}
