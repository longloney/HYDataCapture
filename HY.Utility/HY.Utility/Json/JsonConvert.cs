﻿using System;
using Newtonsoft.Json;

namespace HY.Utility.Json
{
    public static class JsonConvert
    {
      public static string  SerializeObject(object obj)
      {
            Newtonsoft.Json.JsonSerializerSettings setting = new Newtonsoft.Json.JsonSerializerSettings();
            Newtonsoft.Json.JsonConvert.DefaultSettings = new Func<JsonSerializerSettings>(() =>
            {
                setting.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.MicrosoftDateFormat;
                setting.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                return setting;
            });
            string str = Newtonsoft.Json.JsonConvert.SerializeObject(obj, Formatting.Indented, setting).Replace("\r\n","");
            return str;
       }
        public static T DeserializeObject<T>(string value) {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(value);
        }
    }
}
