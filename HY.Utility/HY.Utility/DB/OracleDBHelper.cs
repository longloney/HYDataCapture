﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Text.RegularExpressions;

namespace HY.Utility.DB
{
    public class OracleDBHelper : IDisposable
    {
        private readonly static string _ConnectString;

        private OracleConnection _Connection;
        private OracleTransaction _Transaction;
        private int _CommandTimeout = 0;

        static OracleDBHelper()
        {
            string connstr = System.Configuration.ConfigurationManager.AppSettings["HYConnectionString"];
            if (string.IsNullOrEmpty(connstr))
                throw new Exception("AppSettings不存在HYConnectionString");

            OracleConnectionStringBuilder sb = new OracleConnectionStringBuilder(connstr);
            sb.Pooling = true;
            sb.ConnectionTimeout = 60 * 60;//60分钟
            _ConnectString = sb.ConnectionString;
        }

        private OracleCommand GetCommand(string sql)
        {
            var cmd = new OracleCommand(sql, _Connection);
            cmd.CommandTimeout = 0;
            if (_Connection.State != ConnectionState.Open)
                _Connection.Open();
            return cmd;
        }

        public OracleDBHelper()
        {
            _Connection = new OracleConnection();
            _Connection.ConnectionString = _ConnectString;
            //_Connection.Open();
        }

        public void BeginTrans()
        {
            if (_Connection.State != ConnectionState.Open)
                _Connection.Open();
            _Transaction = _Connection.BeginTransaction();
        }

        public void CommitTrans()
        {
            if (_Transaction != null)
            {
                _Transaction.Commit();
                _Transaction.Dispose();
                _Transaction = null;
            }
        }

        public void AddParametersWithValues(OracleCommand cmd, params object[] param)
        {
            var ms = Regex.Matches(cmd.CommandText, ":[a-zA-Z0-9_]+");
            if (ms != null)
            {
                if (param.Length > 0 )
                {
                    if (ms.Count != param.Length)
                        throw new Exception("参数与参数值数量不相等");
                    var index = 0;
                    foreach (System.Text.RegularExpressions.Match m in ms)
                    {
                        cmd.Parameters.Add(m.Value, param[index]);
                    }
                }
            }
        }

        public void ExecCmd(string sql, params object[] param)
        {
            using (var cmd = GetCommand(sql))
            {
                AddParametersWithValues(cmd, param);
                cmd.ExecuteNonQuery();
            }
        }

        public void AddParametersWithValues(OracleCommand cmd, object param)
        {
            foreach (var p in param.GetType().GetProperties())
            {
                cmd.Parameters.Add(":" + p.Name, p.GetValue(param, null));
            }
        }

        public void ExecCmd(string sql, object param)
        {
            using (var cmd = GetCommand(sql))
            {
                AddParametersWithValues(cmd, param);
                cmd.ExecuteNonQuery();
            }
        }

        public List<T> Query<T>(string sql, object param = null) where T : new()
        {
            List<T> resultlist = new List<T>();
            var dt = GetTable(sql, param);
            foreach (DataRow row in dt.Rows)
            {
                var t = new T();
                foreach (var p in t.GetType().GetProperties())
                {
                    var index = dt.Columns.IndexOf(p.Name);
                    if (index != -1)
                    {
                        if (DBNull.Value == row[index])
                            continue;
                        if (p.PropertyType.IsEnum)
                        {
                            p.SetValue(t, System.Enum.ToObject(p.PropertyType, row[index]), null);
                        }
                        else
                            p.SetValue(t, row[index], null);

                    }
                }
                resultlist.Add(t);
            }
            return resultlist;
        }

        public DataTable GetTable(string sql, object param = null)
        {
            var ds = new DataSet();
            using (var cmd = GetCommand(sql))
            {
                if (param != null)
                    AddParametersWithValues(cmd, param);
                using (OracleDataAdapter adapter = new OracleDataAdapter(cmd))
                {
                    adapter.Fill(ds);
                }
            }
            return ds.Tables[0];
        }

        /// <summary>
        /// 获得单行数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <returns></returns>
        public T Single<T>(string sql, object param = null) where T : new()
        {
            T result = default(T);
            using (var cmd = GetCommand(sql))
            {
                if (param != null)
                    AddParametersWithValues(cmd, param);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        object[] values = new object[reader.FieldCount];
                        var fieldcount = reader.GetValues(values);
                        result = new T();
                        for (int i = 0; i < fieldcount; ++i)
                        {
                            foreach (var p in result.GetType().GetProperties())
                            {
                                if (p.Name == reader.GetName(i))
                                {
                                    if (DBNull.Value != values[i])
                                        p.SetValue(result, values[i], null);
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public object SingeValue(string sql, object param = null)
        {
            object result = null;
            using (var cmd = GetCommand(sql))
            {
                if (param != null)
                    AddParametersWithValues(cmd, param);
                result = cmd.ExecuteScalar();
            }
            return result;
        }

        void IDisposable.Dispose()
        {
            if (_Transaction != null)
            {
                _Transaction.Rollback();
                _Transaction.Dispose();
                _Transaction = null;
            }
            _Connection.Close();
            _Connection.Dispose();
            _Connection = null;
        }
    }
}
