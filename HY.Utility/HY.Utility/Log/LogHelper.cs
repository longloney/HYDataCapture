﻿using System;
using System.Collections.Generic;
using System.Text;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace HY.Utility.Log
{
    public class LogHelper
    {
        public enum LogType
        {
            //Debug=0,
            Error = 1,
            //Fatal=2,
            Info = 3,
            //Warn=4
        }

        public static void WriteErrorLog(Exception ex, Type t)
        {
            WriteLog(LogHelper.LogType.Error, t, "StackTrace:" + System.Environment.NewLine + ex.StackTrace +
                            System.Environment.NewLine +
                            "TargetSite:" + ex.TargetSite +
                            System.Environment.NewLine +
                            "Message:" + ex.Message);
        }

        /// <summary>
        /// 输出日志
        /// </summary>
        /// <param name="logType"></param>
        /// <param name="t"></param>
        /// <param name="msg"></param>
        public static void WriteLog(LogType logType, Type t, string msg)
        {
            log4net.ILog log = log4net.LogManager.GetLogger(t);

            switch (logType)
            {
                case LogType.Error:
                    log.Error(msg);
                    break;
                case LogType.Info:
                    log.Info(msg);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 记录帧
        /// </summary>
        /// <param name="t"></param>
        /// <param name="frames"></param>
        public static void WriteFrame(Type t, byte[] frames)
        {
            if (frames != null && frames.Length > 0)
            {
                log4net.ILog log = log4net.LogManager.GetLogger(t);

                string strFrames = string.Empty;
                for (int i = 0; i < frames.Length; i++)
                {
                    strFrames += frames[i].ToString("X2") + " ";
                }

                log.Debug(strFrames);
            }
        }
    }
}