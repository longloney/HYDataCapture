﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace HY.Utility.Extension
{
    public static class ObjectExtension
    {
        public static IEnumerable<string> GetDescriptions(this object that)
        {
            List<string> output = new List<string>();
            foreach (var p in that.GetType().GetProperties())
            {
                var attrs = p.GetCustomAttributes(false);
                if (attrs.Length > 0)
                    output.Add(((DescriptionAttribute)attrs[0]).Description);
            }
            return output;
        }

        public static string GetDescription(this object that,string propertyName)
        {
            var output = "";
            foreach (var p in that.GetType().GetProperties())
            {
                if (p.Name == propertyName)
                {
                    var attrs = p.GetCustomAttributes(false);
                    if (attrs.Length > 0)
                    {
                        output = ((DescriptionAttribute)attrs[0]).Description;
                        break;
                    }
                }
            }
            return output;
        }
    }
}
