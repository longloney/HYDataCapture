﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HY.Utility.Net;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            HttpHead head = new HttpHead();
            head.Url = "https://taobao.com/";
            head.AllowAutoRedirect = true;
            head.Method = "GET";
            var result = HttpHelper.SendHttpRequest(ref head);

            var data = new
            {
                exParams = new
                {
                    spm = "0.0.0.0",
                    id = "547614000173",
                },
                itemNumId = "547614000173",
            };
            var jsondata = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(data);
            jsondata = System.Web.HttpUtility.UrlEncode(jsondata);
            var url = $"https://acs.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?api=mtop.taobao.detail.getdetail&type=jsonp&dataType=jsonp&data={jsondata}";
            url = "https://acs.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?appKey=12574478&t=1492788363393&sign=abbf13b72eaf6b4c9989f18c2d22b927&api=mtop.taobao.detail.getdetail&v=6.0&ttid=2016%40taobao_h5_2.0.0&isSec=0&ecode=0&AntiFlood=true&AntiCreep=true&H5Request=true&type=jsonp&dataType=jsonp&callback=mtopjsonp1&data=%7B%22exParams%22%3A%22%7B%5C%22id%5C%22%3A%5C%2216056416140%5C%22%2C%5C%22spm%5C%22%3A%5C%220.0.0.0.0pOMbX%5C%22%2C%5C%22abtest%5C%22%3A%5C%2217%5C%22%2C%5C%22rn%5C%22%3A%5C%229c4e4e8269339eed1599d14d98004f71%5C%22%2C%5C%22sid%5C%22%3A%5C%22b6284fd110aca32de4e3257936257f53%5C%22%7D%22%2C%22itemNumId%22%3A%544961927170%22%7D";
            head = new HttpHead() { Url = url, Method = "GET" };
            result = HttpHelper.SendHttpRequest(ref head);
        }

        [TestMethod]
        public void MyTestMethod()
        {
            HYDataCapture.Sdk.AccountService service = new HYDataCapture.Sdk.AccountService();
            var output=service.Login(new HYDataCapture.Domain.Dtos.LoginInputDto() { UserName = "admin", Password = "admin" });
        }
    }
}
