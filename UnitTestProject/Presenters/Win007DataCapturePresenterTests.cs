﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HYDataCapture.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HYDataCapture.Enums;
using HYDataCapture.Views;

namespace HYDataCapture.Presenters.Tests
{
    [TestClass()]
    public class Win007DataCapturePresenterTests : Views.IWin007DataCaptureView
    {
        string IWin007DataCaptureView.Log
        {
            set
            {

            }
        }

        Win007MatchSaveOptionEnum IWin007DataCaptureView.MatchSaveOption
        {
            get
            {
                return Win007MatchSaveOptionEnum.NotSave;
            }
        }

        string[] IWin007DataCaptureView.MatchSaveItems
        {
            set
            {

            }
        }

        List<string> IWin007DataCaptureView.FilterMatch
        {
            get
            {
                return new List<string>(new string[] { "英超", "英冠" });
            }
        }

        int IWin007DataCaptureView.AnalysisDataCaptureProgressValue
        {
            set
            {
            }
        }

        int IWin007DataCaptureView.AnalysisDataCaptureProgressMax
        {
            set
            {
            }
        }

        string IWin007DataCaptureView.WaitTime
        {
            set
            {
                
            }
        }

        string IWin007DataCaptureView.MatchId
        {
            set
            {
                
            }
        }

        [TestMethod()]
        public void Win007DataCapturePresenterTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void DataMenuUrlCaptureTest()
        {
            Win007DataCapturePresenter presenter = new Win007DataCapturePresenter(this);
            Assert.IsTrue(presenter.DataMenuUrlCapture());
        }

        [TestMethod()]
        public void GetTreeNodeDataTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void InitTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void DataCaptureTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void InitDataCaptureWithAnalysisTest()
        {

        }

        [TestMethod()]
        public void GetAnalysisDataTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void DataCaptureWithAnalysisTest()
        {
            Win007DataCapturePresenter presenter = new Win007DataCapturePresenter(this);
            presenter.DataMenuUrlCapture();
            var node = presenter.TreeNodeDataList.Find(s => s.Text == "英超2016-2017");
            presenter.DataCapture(node);
            presenter.DataCaptureWithAnalysis("1247709");
            presenter.FilterCaptureMatch();
        }
    }
}