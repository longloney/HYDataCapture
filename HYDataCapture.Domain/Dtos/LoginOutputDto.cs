﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HYDataCapture.Domain.Dtos
{
    public class LoginOutputDto
    {
        public bool Succeed { get; set; }
        public string Message { get; set; }
    }
}
