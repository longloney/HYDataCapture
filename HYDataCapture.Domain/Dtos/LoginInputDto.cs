﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HYDataCapture.Domain.Dtos
{
    public class LoginInputDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AuthCode { get; set; }
    }
}
