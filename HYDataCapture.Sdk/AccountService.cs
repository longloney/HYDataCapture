﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;

namespace HYDataCapture.Sdk
{
    public class AccountService
    {
        private readonly string BaseApiUrl = System.Configuration.ConfigurationManager.AppSettings["ApiUrl"];
        public AccountService()
        {

        }
        public Domain.Dtos.LoginOutputDto Login(Domain.Dtos.LoginInputDto input)
        {
            IRestClient clt = new RestClient(BaseApiUrl);
            IRestRequest req = new RestRequest("Login",Method.POST);
            req.AddJsonBody(input);
            var rep = clt.Execute<Domain.Dtos.LoginOutputDto>(req);
            return rep.Data;
        }
    }
}
