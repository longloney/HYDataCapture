﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HY.WinForm
{
    public static class DataGridViewExtension
    {
        public static DataTable ToDataTable(this DataGridView dgv, int start_row = -1, int end_row = -1)
        {
            DataTable dt = new DataTable();

            // 列强制转换
            for (int count = 0; count < dgv.Columns.Count; count++)
            {
                DataColumn dc = new DataColumn(dgv.Columns[count].HeaderText.ToString());
                dt.Columns.Add(dc);
            }
            if (start_row == -1 && end_row == -1)
            {
                start_row = 0;
                end_row = dgv.Rows.Count;
            }
            // 循环行
            for (int count = start_row; count < end_row; count++)
            {
                if (dgv.Rows[count].IsNewRow) continue;
                DataRow dr = dt.NewRow();
                for (int countsub = 0; countsub < dgv.Columns.Count; countsub++)
                {
                    dr[countsub] = Convert.ToString(dgv.Rows[count].Cells[countsub].Value);
                }
                dt.Rows.Add(dr);
            }
            return dt;
        }
    }
}
