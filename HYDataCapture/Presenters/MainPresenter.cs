﻿using HY.Utility.Enum;
using HYDataCapture.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture.Presenters
{
    public class MainPresenter
    {
        private IMainView _View;

        public MainPresenter(IMainView view)
        {
            _View = view;
            var items = EnumAttributeHelper.GetEnumDescription(typeof(Enums.MenuEnums)).Where(s => s.Trim().Length > 0);
            _View.Menus = items.ToArray();
        }

        public Enums.MenuEnums GetMenuEnum(string text)
        {
            return EnumAttributeHelper.GetEnumValueFromDescription<Enums.MenuEnums>(text);
        }
    }
}
