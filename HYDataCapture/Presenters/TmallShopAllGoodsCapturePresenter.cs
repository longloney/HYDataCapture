﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HYDataCapture.Views;
using System.Data;
using HY.Utility.Enum;
using HY.Utility.Match;
using HY.Utility.ImportExport;

namespace HYDataCapture.Presenters
{
    public class TmallShopAllGoodsCapturePresenter
    {
        private ITmallShopAllGoodsCaptureView _View;
        

        public TmallShopAllGoodsCapturePresenter(ITmallShopAllGoodsCaptureView view)
        {
            _View = view;
            _View.GoodsTags = EnumAttributeHelper.GetEnumDescription(typeof(Enums.TmallGoodsTagEnum)).ToArray();
            Data = new DataTable();
        }

       public Enums.CaptureStateEnum DataCapture()
        {
            var output = Enums.CaptureStateEnum.NoMatch;

            do
            {
                if (_View.SelectGoodsTags.Length == 0)
                {
                    _View.Log = "未选中任何商品Tag，无法采集";
                    break;
                }
                string html = _View.DataHtml;

                List<Enums.TmallGoodsTagEnum> tagList = new List<Enums.TmallGoodsTagEnum>();
                foreach (var tag in _View.SelectGoodsTags)
                {
                    tagList.Add(EnumAttributeHelper.GetEnumValueFromDescription<Enums.TmallGoodsTagEnum>(tag));
                    if (Data.Columns.IndexOf(tag) == -1)
                        Data.Columns.Add(tag, typeof(string));
                }

                HtmlElementMatcher matcher = new HtmlElementMatcher(html);
                bool isEnd = false;

                bool isLazyLoading = false;
                //data-ks-lazyload
                HtmlElementMatcher.SelectNodesEventHandler handler2 = (index, items, outerhtml) => {
                    var matcher2 = new HtmlElementMatcher(outerhtml);
                    DataRow row = Data.NewRow();
                    for (var i = 0; i < tagList.Count; ++i)
                    {
                        string value = "";
                        switch (tagList[i])
                        {
                            case Enums.TmallGoodsTagEnum.Id:
                                {
                                    value = matcher2.GetNodeAttributeValue("//dl", "data-id");
                                }
                                break;
                            case Enums.TmallGoodsTagEnum.Title:
                                {
                                    value = matcher2.GetNodeAttributeValue("//dt[@class='photo']//img", "alt");
                                }
                                break;
                            case Enums.TmallGoodsTagEnum.Link:
                                {
                                    value = matcher2.GetNodeAttributeValue("//dd[@class='detail']/a", "href");
                                    if (!value.StartsWith("http:") && !value.StartsWith("https:"))
                                        value = "http:" + value;
                                }
                                break;
                            case Enums.TmallGoodsTagEnum.ImageLink:
                                {
                                    var attr = "src";
                                    if (isLazyLoading)
                                        attr = "data-ks-lazyload";
                                    value = matcher2.GetNodeAttributeValue("//dt[@class='photo']//img", attr);
                                    if (!value.StartsWith("http:") && !value.StartsWith("https:"))
                                        value = "http:" + value;
                                }
                                break;
                            case Enums.TmallGoodsTagEnum.Price:
                                {
                                    value = matcher2.GetNodeInnerText("//span[@class='c-price']").Trim();
                                }
                                break;
                            case Enums.TmallGoodsTagEnum.SaleNum:
                                {
                                    value = matcher2.GetNodeInnerText("//span[@class='sale-num']");
                                }
                                break;
                            case Enums.TmallGoodsTagEnum.Comment:
                                {
                                    value = matcher2.GetNodeInnerText("//*[@class='rates']");
                                    var m = System.Text.RegularExpressions.Regex.Match(value, @"\d+");
                                    if (m.Success)
                                        value = m.Value;
                                }
                                break;
                        }
                        row[i] = value;
                    }
                    Data.Rows.Add(row);
                };


                HtmlElementMatcher.SelectNodesEventHandler handler = (index, items, outerhtml) =>
                 {
                     var matcher2 = new HtmlElementMatcher(outerhtml);
                     switch(matcher2.GetCurrentNodeAttributeValue("class"))
                     {
                         case "pagination":
                             isEnd = true;
                             break;
                     }
                     if (isEnd)
                         return;
                     if (index == 1)
                         isLazyLoading = true;

                     matcher2.GetNodesAttributeValues(handler2, "//dl[contains(@class,'item')]");
                 };
                matcher.GetNodesAttributeValues(handler, "//div[@class='J_TItems']/div");

                NextUrl = matcher.GetNodeAttributeValue("//a[contains(text(),'下一页')]", "href");
                if (!string.IsNullOrEmpty(NextUrl))
                {
                    if (!NextUrl.StartsWith("http:") && !NextUrl.StartsWith("https:"))
                        NextUrl = "https:" + NextUrl;
                    output = Enums.CaptureStateEnum.Success;
                }
                else
                    output = Enums.CaptureStateEnum.Completed;
            } while (false);
            SetSalesAmount();
            return output;
        }

        public void ExportExcel(string fileName)
        {
            try
            {
                if (Data == null)
                {
                    _View.Log = "没有数据可供导出";
                    return;
                }
                _View.Log = "正在导出...";
                using (var helper = new ExcelHelper(fileName))
                {
                    helper.DataTableToExcel(Data, "导出数据", true);
                }
                _View.Log = "已导出：" + fileName;
            }
            catch (Exception ex)
            {
                _View.Log = "导出出错：" + ex.Message;
            }
        }

        public void ExportExcelWithGoodsImage(string fileName)
        {
            try
            {
                do
                {
                    if (Data == null)
                    {
                        _View.Log = "没有数据可供导出";
                        break;
                    }
                    var tag = Enums.TmallGoodsTagEnum.ImageLink.GetEnumDescription();
                    var index = Data.Columns.IndexOf(tag);
                    if (index == -1)
                    {
                        _View.Log = "不存在Tag，无法导出："+tag;
                        break;
                    }
                    _View.Log = "正在导出...";
                    using (var helper = new ExcelHelper(fileName))
                    {
                        helper.DataTableToExcel(Data, new List<int>(new int[] { index }));
                    }
                    _View.Log = "已导出：" + fileName;
                } while (false);
            }
            catch (Exception ex)
            {
                _View.Log = "导出出错：" + ex.Message;
            }
        }

        private bool CheckSaleNumPriceTagExists()
        {
            bool output = false;
            do
            {
                if (!Data.Columns.Contains(Enums.TmallGoodsTagEnum.SaleNum.GetEnumDescription()))
                {
                    break;
                }
                if (!Data.Columns.Contains(Enums.TmallGoodsTagEnum.Price.GetEnumDescription()))
                {
                    break;
                }
                output = true;
            } while (false);
            return output;
        }

        private void SetSalesAmount()
        {
            var output = "";
            do
            {
                if (Data.Columns.Count == 0)
                    break;
                if (!CheckSaleNumPriceTagExists())
                    break;

                var total = 0m;
                var temp = 0m;
                foreach (DataRow row in Data.Rows)
                {
                    if (!decimal.TryParse(Convert.ToString(row[Enums.TmallGoodsTagEnum.SaleNum.GetEnumDescription()]), out temp))
                        continue;
                    var saleNum = temp;
                    if (!decimal.TryParse(Convert.ToString(row[Enums.TmallGoodsTagEnum.Price.GetEnumDescription()]), out temp))
                        continue;
                    var price = temp;
                    total += (price * saleNum);
                }
                output = total.ToString();
            } while (false);
            _View.SaleAmount = output;
        }

        public string NextUrl { get; private set; }
        public DataTable Data { get; private set; }
    }
}
