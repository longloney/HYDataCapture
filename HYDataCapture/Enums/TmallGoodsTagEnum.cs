﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace HYDataCapture.Enums
{
    public enum TmallGoodsTagEnum
    {
        [Description("编号")]
        Id,
        [Description("标题")]
        Title,
        [Description("链接")]
        Link,
        [Description("图片链接")]
        ImageLink,
        [Description("价格")]
        Price,
        [Description("已售件数")]
        SaleNum,
        [Description("评论")]
        Comment
    }
}
