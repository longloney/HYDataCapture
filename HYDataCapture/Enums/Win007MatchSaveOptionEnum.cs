﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace HYDataCapture.Enums
{
    public enum Win007MatchSaveOptionEnum
    {
        [Description("不保留已采集联赛数据")]
        NotSave,
        [Description("保留已采集联赛数据")]
        Save,
    }
}
