﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace HYDataCapture.Enums
{
    public enum MenuEnums
    {
        //[Description("淘宝店铺所有商品采集")]
        [Description("")]
        TaobaoShopAllGoodsCapture,

        //[Description("天猫店铺所有商品采集")]
        [Description("")]
        TmallShopAllGoodsCapture,

        //[Description("速卖通店铺所有商品采集")]
        [Description("")]
        AliexpressShopAllGoodsCapture,

        [Description("Win007.com数据采集")]
        Win007DataCapture,
    }
}
