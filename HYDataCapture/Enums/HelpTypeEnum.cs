﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture.Enums
{
    public enum HelpTypeEnum
    {
        /// <summary>
        /// 什么是淘宝店铺Url
        /// </summary>
        WhatIsTaobaoShopUrl = 0,

        /// <summary>
        /// 什么是天猫店铺Url
        /// </summary>
        WhatIsTmallShopUrl,

        /// <summary>
        /// 什么是速卖通店铺Url
        /// </summary>
        AliexpressShopUrl,
    }
}
