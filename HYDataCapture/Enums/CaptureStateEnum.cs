﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture.Enums
{
    public enum CaptureStateEnum
    {
        Success,
        Completed,
        NoMatch,
        Exception,
    }
}
