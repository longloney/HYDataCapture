﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace HYDataCapture.Enums
{
    public enum TaobaoGoodsTagEnum
    {
        [Description("编号")]
        Id,
        [Description("标题")]
        Title,
        [Description("链接")]
        Link,
        [Description("图片链接")]
        ImageLink,
        [Description("价格")]
        Price,
        [Description("已售件数")]
        SaleNum,
        [Description("评论")]
        Comment
    }
}
