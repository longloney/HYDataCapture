﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture.Views
{
    public interface ILogView
    {
        void WriteLog(string text);
    }
}
