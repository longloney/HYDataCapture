﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture.Views
{
    public interface IAliexpressShopAllGoodsCaptureView
    {
        string[] GoodsTags { set; }
        string Log { set; }
        string[] SelectGoodsTags { get; }
        string DataHtml { get; }
        string SaleAmount { set; }
    }
}
