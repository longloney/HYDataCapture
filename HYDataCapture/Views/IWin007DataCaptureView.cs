﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture.Views
{
    public interface IWin007DataCaptureView
    {
        string Log { set; }

        Enums.Win007MatchSaveOptionEnum MatchSaveOption { get;  }

        string[] MatchSaveItems { set; }

        List<string> FilterMatch { get; }

        int AnalysisDataCaptureProgressValue { set; }
        int AnalysisDataCaptureProgressMax { set; }

        string WaitTime { set; }

        string MatchId { set; }
    }
}
