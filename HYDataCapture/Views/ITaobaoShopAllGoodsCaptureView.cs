﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture.Views
{
    public interface ITaobaoShopAllGoodsCaptureView
    {
        string[] GoodsTags { set; }
        string[] SelectGoodsTags { get; }

        string DataHtml { get; }

        string Log { set; }
        string SaleAmount { set; }

        int CurrentIndex { set; }
    }
}
