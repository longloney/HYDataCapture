﻿using HYDataCapture.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HYDataCapture.Dialogs
{
    public partial class AboutDialog : Form
    {
        public AboutDialog()
        {
            InitializeComponent();
        }

        private void AboutDialog_Load(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"{Defines.AppName} {Defines.Version}");
            sb.AppendLine("");
            sb.AppendLine($"开发者：{Defines.Author}");
            sb.AppendLine($"联系方式：{Defines.Contact}");
            sb.AppendLine($"讨论群：{Defines.QQGroup}");
            richTextBox1.Text = sb.ToString();
            richTextBox1.SelectionStart = richTextBox1.Text.Length;
        }
    }
}
