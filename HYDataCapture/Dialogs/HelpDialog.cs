﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HYDataCapture.Dialogs
{
    public partial class HelpDialog : Form
    {
        public HelpDialog(Enums.HelpTypeEnum type)
        {
            InitializeComponent();

            var selectindex = (int)type;
            TabPage showpage = null;
            if (selectindex >= 0 && selectindex < tabControl1.TabPages.Count)
                showpage = tabControl1.TabPages[selectindex];
            if(showpage!=null)
            {
                List<TabPage> pageList = new List<TabPage>();
                foreach (TabPage item in tabControl1.TabPages)
                    pageList.Add(item);
                pageList.ForEach(s => {
                    if (s != showpage)
                        tabControl1.TabPages.Remove(s);
                });
            }
        }
    }
}
