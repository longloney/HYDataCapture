﻿using HYDataCapture.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HYDataCapture.Dialogs
{
    public partial class LoginDialog : Form
    {
        public LoginDialog()
        {
            InitializeComponent();

            textBox1.Text = "admin";
            textBox2.Text = "admin";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.TextLength == 0 || textBox2.TextLength == 0)
                    throw new Exception("帐号和密码不能为空");
                Sdk.AccountService service = new Sdk.AccountService();
                var output = service.Login(new Domain.Dtos.LoginInputDto() { UserName = textBox1.Text, Password = textBox2.Text });
                if (output.Succeed)
                {
                    Hide();
                    using (var main = new MainForm())
                    {
                        main.ShowDialog();
                    }
                    Close();
                }
                else
                {
                    MessageBox.Show(output.Message);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox2.Focus();
            }
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1.PerformClick();
            }
        }
    }
}
