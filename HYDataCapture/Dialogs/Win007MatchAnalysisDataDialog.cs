﻿using HY.Utility.Extension;
using HY.WinForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HYDataCapture.Dialogs
{
    public partial class Win007MatchAnalysisDataDialog : Form
    {
        private DataTable _list;
        public Win007MatchAnalysisDataDialog(DataTable list)
        {
            InitializeComponent();

            _list = list;

        }

        private void Win007MatchAnalysisDataDialog_Load(object sender, EventArgs e)
        {
            using (DataGridViewBindData bd = new DataGridViewBindData(dataGridView1, delegate
            {
                return _list;
            })) { }
            this.toolStripStatusLabel1.Text = $"记录数：{_list.Rows.Count}";
        }
    }
}
