﻿using HYDataCapture.Models;
using HYDataCapture.Presenters;
using HYDataCapture.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HYDataCapture.Forms
{
    public partial class MainForm : Form, IMainView
    {
        private MainPresenter _Presenter;
        private Dictionary<Enums.MenuEnums,BaseForm> _MenuDic;
        private LogForm _LogForm;
        public MainForm()
        {
            InitializeComponent();

            Text = Defines.AppName + " " + Defines.Version;

            _MenuDic = new Dictionary<Enums.MenuEnums, BaseForm>();
            _LogForm = new LogForm();

            _Presenter = new MainPresenter(this);

        }

        string[] IMainView.Menus
        {
            set
            {
                var root = treeView1.Nodes.Add("所有功能");
                foreach(var item in value)
                {
                    var node = root.Nodes.Add(item);
                    node.ImageIndex = 1;
                }
                root.ExpandAll();
            }
        }

        private void treeView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right)
            {
                return;
            }
            var node = treeView1.GetNodeAt(e.X, e.Y);
            if (node != null)
            {
                if (node.Level == 1)
                {
                    treeView1.ContextMenuStrip = contextMenuStrip1;
                }
            }
        }

        private void 打开ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var node = treeView1.SelectedNode;
            if (node != null)
            {
                if (node.Level == 1)
                {
                    string text = node.Text;
                    var menu = _Presenter.GetMenuEnum(text);
                    BaseForm form = null;
                    if (_MenuDic.ContainsKey(menu))
                        form = _MenuDic[menu];
                    else
                    {
                        switch (menu)
                        {
                            case Enums.MenuEnums.TaobaoShopAllGoodsCapture:
                                {
                                    form = new TaobaoShopAllGoodsCaptureForm(_LogForm);
                                }
                                break;
                            case Enums.MenuEnums.TmallShopAllGoodsCapture:
                                {
                                    form = new TmallShopAllGoodsCaptureForm(_LogForm);
                                }
                                break;
                            case Enums.MenuEnums.AliexpressShopAllGoodsCapture:
                                {
                                    form = new AliexpressShopAllGoodsCaptureForm(_LogForm);
                                }
                                break;
                            case Enums.MenuEnums.Win007DataCapture:
                                {
                                    form = new Win007DataCaptureForm(_LogForm);
                                }
                                break;
                        }
                        _MenuDic[menu] = form;
                    }
                    if (form==null)
                    {
                        MessageBox.Show("不支持的功能");
                    }
                    else
                    {
                        form.Text = text;
                        _LogForm.Show(dockPanel1, WeifenLuo.WinFormsUI.Docking.DockState.DockBottom);
                        form.Show(dockPanel1);  
                    }
                }
            }
        }

        private void 关于本程序ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (Dialogs.AboutDialog dlg = new Dialogs.AboutDialog())
            {
                dlg.ShowDialog();
            }
        }
    }
}
