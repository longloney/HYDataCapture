﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace HYDataCapture.Forms
{
    public class Utility
    {
        public string ErrorMessage { get; private set; }
        public string ShopAllGoodsUrl { get; set; }
        public bool ShopUrlCheck(string shopurl,Enums.MenuEnums menu)
        {
            do
            {
                ErrorMessage = "";
                if (string.IsNullOrEmpty(shopurl))
                {
                    ErrorMessage = "店铺Url不能为空";
                    break;
                }
                if (!shopurl.StartsWith("https://"))
                {
                    ErrorMessage = "错误的店铺Url";
                    break;
                }
                shopurl = shopurl.TrimEnd('/');
                switch(menu)
                {
                    case Enums.MenuEnums.TaobaoShopAllGoodsCapture:
                        {
                            if (!shopurl.EndsWith("taobao.com"))
                            {
                                ErrorMessage = "错误的店铺Url";
                                break;
                            }
                            ShopAllGoodsUrl = shopurl + "/search.htm";
                        }
                        break;
                    case Enums.MenuEnums.TmallShopAllGoodsCapture:
                        {
                            if (!shopurl.EndsWith("tmall.com"))
                            {
                                ErrorMessage = "错误的店铺Url";
                                break;
                            }
                            ShopAllGoodsUrl = shopurl + "/search.htm";
                        }
                        break;
                    case Enums.MenuEnums.AliexpressShopAllGoodsCapture:
                        {
                            var m = Regex.Match(shopurl, @"https://[^\.]+\.aliexpress.com/store/\d+");
                            if(!m.Success)
                            {
                                ErrorMessage = "错误的店铺Url";
                                break;
                            }
                            m = Regex.Match(shopurl, @"https://[^\.]+\.aliexpress.com/store/");
                            if (m.Success)
                            {
                                ShopAllGoodsUrl = $"{m.Value}all-wholesale-products/{shopurl.Replace(m.Value, "")}.html";
                            }
                        }
                        break;
                }
                if (ErrorMessage.Length > 0)
                    break;

                return true;
            } while (false);
            return false;
        }
    }
}
