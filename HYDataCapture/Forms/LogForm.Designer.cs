﻿namespace HYDataCapture.Forms
{
    partial class LogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Rtx_Log = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // Rtx_Log
            // 
            this.Rtx_Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Rtx_Log.Location = new System.Drawing.Point(0, 0);
            this.Rtx_Log.Name = "Rtx_Log";
            this.Rtx_Log.ReadOnly = true;
            this.Rtx_Log.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.Rtx_Log.Size = new System.Drawing.Size(626, 256);
            this.Rtx_Log.TabIndex = 1;
            this.Rtx_Log.Text = "";
            // 
            // LogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 256);
            this.Controls.Add(this.Rtx_Log);
            this.Name = "LogForm";
            this.Text = "运行日志";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox Rtx_Log;
    }
}