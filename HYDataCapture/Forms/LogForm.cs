﻿using HYDataCapture.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HYDataCapture.Forms
{
    public partial class LogForm : BaseForm, ILogView
    {
        public LogForm()
        {
            InitializeComponent();
        }

        void ILogView.WriteLog(string text)
        {
            string inserttext = "\r\n";
            this.Invoke((MethodInvoker)delegate {
                if (Rtx_Log.Lines.Length > 1000)
                {
                    Rtx_Log.Clear();
                    inserttext += "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] 已清除日志...\r\n";
                }
                inserttext = "[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + text + inserttext + Rtx_Log.Text;
                Rtx_Log.Text = inserttext;
                Application.DoEvents();
            });
        }
    }
}
