﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture.Models
{
    public class Win007TreeNodeDataIndexModel
    {
        public string Id { get; set; }
        public Win007TreeNodeDataModel Node { get; set; }
    }
}
