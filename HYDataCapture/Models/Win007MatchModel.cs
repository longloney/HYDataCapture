﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace HYDataCapture.Models
{
    public class Win007MatchModel
    {
        [Description("轮次")]
        public string LunCi { get; set; }

        [Description("时间")]
        public string Time { get; set; }

        [Description("主队")]
        public string HomeTeam { get; set; }

        [Description("比分")]
        public string Score { get; set; }

        [Description("客队")]
        public string VisitingTeam { get; set; }

        [Description("让球全场")]
        public string RangQiuQuanChang { get; set; }

        [Description("胜负平")]
        public string MatchResult { get; set; }

        [Description("主队近十场的胜场数")]
        public int HomeTeam10Win { get; set; }

        [Description("客队近十场的胜场数")]
        public int VisitingTeam10Win { get; set; }

        [Description("解析主队")]
        public string AnalysisHomeTeam { get; set; }

        [Description("解析客队")]
        public string AnalysisVisitingTeam { get; set; }

        [Description("赛事编号")]
        public string Id { get; set; }

        [Description("状态")]
        public int State { get; set; }


    }
}
