﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace HYDataCapture.Models
{
    public class Win007TreeNodeDataModel
    {
        private string _Id = Guid.NewGuid().ToString();
        public string Id { get { return _Id; } set { _Id = value; } }
        public string Text { get; set; }
        public bool IsOnlyTitle { get; set; }
        public string Url { get; set; }
        public List<Win007TreeNodeDataModel> ChildNodeList { get; set; }
    }
}
