﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace HYDataCapture.Models
{
    public class Win007MatchResultModel
    {
        [Description("赛事ID")]
        public string MatchId { get; set; }

        [Description("赛事")]
        public string Type { get; set; }
        [Description("日期")]
        public string Date { get; set; }
        [Description("主队")]
        public string HomeTeam { get; set; }
        [Description("客队")]
        public string VisitingTeam { get; set; }
        [Description("比分")]
        public string Score { get; set; }

        [Description("胜负")]
        public string Result { get; set; }

        [Description("是否主队")]
        public bool IsHomeTeam { get; set; }
    }
}
