﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture.Models
{
    public class GetAnalysisDataSearchOption
    {
        public List<string> TypeList { get; set; }
    }
}
