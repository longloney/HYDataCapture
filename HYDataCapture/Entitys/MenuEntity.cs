﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture.Entitys
{
    public class MenuEntity
    {
        public string Id { get; set; }
        public string Pid { get; set; }
        public long Level { get; set; }
        public string Text { get; set; }
        public long SN { get; set; }
    }
}
