﻿using HYDataCapture.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYDataCapture
{
    public class BaseForm : WeifenLuo.WinFormsUI.Docking.DockContent
    {
        protected readonly ILogView _Log;

        public BaseForm(ILogView log) : this()
        {
            _Log = log;
        }

        public BaseForm()
        {
            HideOnClose = true;
        }
    }
}
